export default class etherscan {
  static transactions(etherAddress, apiKey = null) {
    var url = "http://api.etherscan.io/api?module=account&action=txlist&address=" + etherAddress;
    if(apiKey)
      url += "&apikey=" + apiKey;

    var promise = new Promise((resolve, reject)=>{
      fetch(url).then(r=>{
        return r.json();
      }).then(r=>{
        if(r.status === "1") {
          var result = [];
          r.result.forEach(t=>{
            if(t.isError === "0") {
              result.push(t);
            }
          });
          resolve(result);
        }
        else {
          reject(r.message);
        }
      });
    });
    return promise;
  }
}
